package utils.model;

public class Consts {
    private Consts(){}
    public static final String IMPORTANT_FOLDER_SEARCH_VALUE = "is:important ";
    public static final int LETTER_AMOUNT_TO_DELETE = 3;
}

