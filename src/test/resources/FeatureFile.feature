Feature: GmailTest
  Scenario Outline: User login to gmail, mark 3 last letters as important, and delete them from important folder
    Given User is on https://gmail.com
    When User login using email "<email>" and password "<password>"
    Then User is in his home page
    And mark 3 last letter as important using important button
    And navigate to important folder
    And select last 3 messages using checkboxes
    And press delete button
    Examples:
     | email                 | password         |
     | 0673404783r@gmail.com | 140992musepa     |