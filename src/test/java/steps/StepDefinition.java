package steps;

import businessObjects.LoginBO;
import businessObjects.MessagesBO;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.DriverLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import utils.PropertyUtils;

public class StepDefinition {
    private LoginBO loginBO = new LoginBO();
    private MessagesBO messagesBO = new MessagesBO();
    private Logger logger = LogManager.getLogger(StepDefinition.class);

    @Given("^User is on https://gmail\\.com$")
    public void user_is_on_https_gmail_com() {
        DriverLoader.getDriver().get(PropertyUtils.getConfigList().get("cite"));
    }

    @When("^User login using email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void user_login_using_email_and_password(String email, String password) {
        loginBO.login(email, password);
    }

    @Then("^User is in his home page$")
    public void userInHomePage() {
        logger.info("User visit home page");
    }

    @And("^mark (\\d+) last letter as important using important button$")
    public void mark_last_letter_as_important_using_important_button(int amount) {
        messagesBO.markAsImportant(amount);
        Assert.assertTrue(messagesBO.isMarkAsImportantLabelPresent());
    }

    @And("^navigate to important folder$")
    public void navigate_to_important_folder() {
        messagesBO.openImportantFolder();
    }

    @And("^select last (\\d+) messages using checkboxes$")
    public void select_last_messages_using_checkboxes(int amount) {
        messagesBO.selectMessagesInImpFolder(amount);
    }

    @And("^press delete button$")
    public void press_delete_button() {
        int messAmountBeforeDeleting = messagesBO.checkLettersAmount();
        messagesBO.deleteMessages();
        int messAmountAfterDeleting = messagesBO.checkLettersAmount();
        Assert.assertNotEquals(messAmountAfterDeleting, messAmountBeforeDeleting);
    }

    @AfterClass
    public void quitDriver() {
        DriverLoader.tearDown();
    }
}
